-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: 192.168.0.98    Database: lora
-- ------------------------------------------------------
-- Server version	5.5.5-10.0.36-MariaDB-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_11_06_110416_create_tasks_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(200) NOT NULL,
  `description` longtext,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `updated_at` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,'Таск менеджер','Создать простой таск-менеджер для повседневных задач','2018-11-11 09:29:28.000000','2018-11-11 09:29:12.046942',1),(2,'TODO SMTH','You must to do something Big and beautiful','2018-11-11 09:29:28.000000','2018-11-11 09:29:28.000000',1),(3,'test','adasdasdasd','2018-11-11 09:29:28.000000','2018-11-11 09:29:28.000000',1),(11,'DENVER','You must to do something Big and beautifu333333333333333333333333333l','2018-11-11 09:29:28.000000','2018-11-11 13:31:44.000000',0),(12,'TEST DELETE4567','You3333','2018-11-12 09:20:32.000000','2018-11-12 09:27:12.000000',0),(15,'TEST DELETE45697','You must to do something Big and beautiful','2018-11-12 09:41:01.000000','2018-11-12 09:41:01.000000',1);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `project_id` int(10) DEFAULT NULL,
  `end_date` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  `status` tinyint(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (1,'tes1','do smth','2018-11-11 09:29:28.000000',1,'2018-11-11 09:29:28.000000',0,'2018-11-11 09:29:28','2018-11-11 09:29:28'),(2,'tes2','learn JavaScript','2018-11-11 09:54:38.914078',2,'2018-11-11 09:29:28.000000',1,'2018-11-11 09:29:28','2018-11-11 09:29:28'),(3,'tes3','learn React','2018-11-11 09:54:38.914242',1,'2018-11-11 09:29:28.000000',2,'0000-00-00 00:00:00','2018-11-11 09:29:28'),(4,'tes4','learn Redux','2018-11-11 09:54:38.914402',2,'2018-11-11 09:29:28.000000',3,'2018-11-11 09:29:28','2018-11-11 09:29:28'),(5,'Task1','Decription task example','2018-11-11 11:29:28.000000',1,'2018-11-11 11:29:28.000000',4,'2018-11-11 10:09:56','2018-11-11 10:09:56'),(6,'Task1','Decription task example','2018-11-11 11:29:28.000000',1,'2018-11-11 11:29:28.000000',4,'2018-11-11 10:10:23','2018-11-11 10:10:23'),(7,'Task_EDITED','Decription task example EDITED','2018-11-11 10:16:35.586123',1,'2018-11-11 11:29:28.000000',1,'2018-11-11 10:10:47','2018-11-11 10:16:35'),(28,'Denisa','Decription task example','2018-11-11 11:29:28.000000',3,'2018-11-11 11:29:28.000000',4,'2018-11-12 09:59:47','2018-11-12 09:59:47'),(33,'Denisa','Decription task example','2018-11-11 11:29:28.000000',3,'2018-11-11 11:29:28.000000',4,'2018-11-12 10:01:17','2018-11-12 10:01:17'),(34,'weqweqwe','Decription task example','2018-11-11 11:29:28.000000',1,'2018-11-11 11:29:28.000000',4,'2018-11-12 10:12:27','2018-11-12 10:12:27'),(35,'weqweqwe','Decription task example','2018-11-11 11:29:28.000000',2,'2018-11-11 11:29:28.000000',4,'2018-11-12 10:23:23','2018-11-12 10:23:23'),(36,'123','www45','2018-11-12 13:41:21.039668',7,'2018-11-11 11:29:28.000000',3,'2018-11-12 10:23:51','2018-11-12 13:41:21'),(38,'Create WEB Interface','Decription task example','2018-11-11 11:29:28.000000',1,'2018-11-11 11:29:28.000000',0,'2018-11-12 15:06:26','2018-11-12 15:06:26'),(39,'Create Admin Panel','Decription task example','2018-11-11 11:29:28.000000',1,'2018-11-11 11:29:28.000000',0,'2018-11-12 15:07:02','2018-11-12 15:07:02'),(40,'Create DataBase','Decription task example','2018-11-11 11:29:28.000000',1,'2018-11-11 11:29:28.000000',0,'2018-11-12 15:07:35','2018-11-12 15:07:35'),(41,'Create UI','Decription task example','2018-11-11 11:29:28.000000',1,'2018-11-11 11:29:28.000000',0,'2018-11-12 15:08:41','2018-11-12 15:08:41'),(42,'Create SEO ','Decription task example','2018-11-11 11:29:28.000000',1,'2018-11-11 11:29:28.000000',0,'2018-11-12 15:09:15','2018-11-12 15:09:15'),(43,'Create SMTHING ','Decription task example','2018-11-11 11:29:28.000000',1,'2018-11-11 11:29:28.000000',0,'2018-11-12 15:30:00','2018-11-12 15:30:00'),(44,'Create SMTHING 2 ','Decription task example','2018-11-11 11:29:28.000000',1,'2018-11-11 11:29:28.000000',0,'2018-11-12 15:30:07','2018-11-12 15:30:07');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks_user_relations`
--

DROP TABLE IF EXISTS `tasks_user_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks_user_relations` (
  `user_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks_user_relations`
--

LOCK TABLES `tasks_user_relations` WRITE;
/*!40000 ALTER TABLE `tasks_user_relations` DISABLE KEYS */;
INSERT INTO `tasks_user_relations` VALUES (333,444),(1,11),(15,12),(12,36),(17,38),(17,39),(17,40),(17,41),(17,42),(20,43),(20,44);
/*!40000 ALTER TABLE `tasks_user_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Denis','Miden@gmail.com','$2y$10$AdkKuoUCoddHiJvjD2gTyOlPjhjFoM7PfQme5Cnvr2YXHOaQF.fHe','MUyLUrZDt7Cj0hljglDGiN61Lg611uYNloHVjNmLto3dlWhAq4YSJajSzQpq','2018-11-06 11:59:15','2018-11-06 11:59:15'),(2,'John','jpnh@mail.com','$2y$10$AdkKuoUCoddHiJvjD2gTyOlPjhjFoM7PfQme5Cnvr2YXHOaQF.fHe','MUyLUrZDt7Cj0hljglDGiN61Lg611uYNloHVjNmLto3dlWhAq4YSJajSzQpq','2018-11-06 11:59:15','2018-11-06 11:59:15'),(3,'Max','max@max.com','$2y$10$AdkKuoUCoddHiJvjD2gTyOlPjhjFoM7PfQme5Cnvr2YXHOaQF.fHe$2y$10$AdkKuoUCoddHiJvjD2gTyOlPjhjFoM7PfQme5Cnvr2YXHOaQF.fHe','MUyLUrZDt7Cj0hljglDGiN61Lg611uYNloHVjNmLto3dlWhAq4YSJajSzQpq','2018-11-06 11:59:15','2018-11-06 11:59:15'),(17,'asd','asd@asd.asd','$2y$10$ZOJOUha5CQLn4FY2U/mZr.Cvu2aa7FIS9HaBu4OgM.LOGzq8qJKfi',NULL,'2018-11-10 09:57:06','2018-11-10 09:57:06'),(18,'Max33','max33@max.com','$2y$10$snqVawZOzgwgmUhgQl91O.uB9kymvoD.qcvIjG.NyX6FjbyZpHBPm',NULL,'2018-11-11 08:50:33','2018-11-11 08:50:33'),(19,'Samuel','samuel@gmail.com','$2y$10$cBfRLYUn2wbV/oWiXTlJW.Mn9Q/cq8g6ZSKN7Wtpz/YitYMcE8TJS',NULL,'2018-11-11 12:13:20','2018-11-11 12:13:20'),(20,'SamuelDENVER','samuelDENVER@gmail.com','$2y$10$LYR5dANwj.idhFtfVK1yxODTvVTtMHb9wZA6ksXAf/Q6ORIx7b91i',NULL,'2018-11-11 13:28:29','2018-11-11 13:28:29');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-12 17:44:56
