<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/




$router->get('/', function () use ($router) {
     return 'Hello Denis';
});

$router->get('/key', function () {
     return str_random(32);
});

$router->get('/', ['uses'=> 'UsersController@index']);

$router->group(['middleware' => 'auth'], function () use ($router) {

    $router->get('/users', ['uses'=> 'UsersController@users']);

    $router->get('/users/{id}', ['uses'=> 'UsersController@user']);

    $router->post('/users', ['uses'=> 'UsersController@createUser']);

    $router->post('/users/login', ['uses'=> 'UsersController@getToken']);



    /* projects routing*/

    $router->get('/projects', ['uses'=> 'ProjectsController@index']);

    $router->post('/project', ['uses'=> 'ProjectsController@store']);
    $router->get('/project/create-project', ['uses'=> 'ProjectsController@createProject']);
  /*   $router->get('/project/edit-project/{id}', ['uses'=> 'ProjectsController@editProject']); */
    $router->get('/project/{id}', ['uses'=> 'ProjectsController@show']);
    $router->put('/project/{id}', ['uses'=> 'ProjectsController@update']);
    $router->delete('/project/{id}', ['uses'=> 'ProjectsController@destroy']);


    /* tasks routing*/

    /* $router->get('/tasks', ['uses'=> 'TasksController@index']); */
    /* with filter */
    $router->post('/tasks', ['uses'=> 'TasksController@index']);

    $router->get('/task/{id}', ['uses'=> 'TasksController@show']);

    $router->post('/task', ['uses'=> 'TasksController@store']);

    $router->put('/task/{id}', ['uses'=> 'TasksController@update']);

    $router->delete('/task/{id}', ['uses'=> 'TasksController@destroy']);

});


$router->post('/login', 'AuthController@postLogin');
$router->get('/login', 'AuthController@loginForm');

$router->get('/register', 'UsersController@registerForm');
$router->post('/register', 'UsersController@register');




