@extends('home')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="btn-group group_refs">
                        <a href="/projects">Projects list</a>
                      <!--   <span class="glyphicon glyphicon-plus"></span>
                        <a href="/project">Add project</a> -->
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Description</th>
                    </tr>
                    <ul>
                        @foreach($projects as $i=>$project)
                        <tr>
                            <td><b>{{$i+1}}</b></td>
                            <td><b><a href="/project/{{$project->id}}"> {{$project->project_name}} </a></b></td>
                            <td><i>{{$project->description}}</i></td>
                            <td> <a href="/project/{{$project->id}}" > edit</a> | delete </td>
                            
                        </tr>
                        @endforeach
                    </ul>

                    </table>

                    <a href="/project/create-project">   Add new project </a>

                   
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
                