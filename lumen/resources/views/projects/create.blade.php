@extends('home')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="btn-group group_refs">
                        <a href="/projects">Projects list</a>
                      <!--   <span class="glyphicon glyphicon-plus"></span>
                        <a href="/project">Add project</a> -->
                    </div>
                </div>

                <div class="panel-body">
                <form action="/project" method="post">

                    <div class="form-group">
                        <label for="project_name" class="col-md-4 control-label">Project Name</label>
                        <div class="col-md-6">
                            <input id="project_name" type="text" class="form-control" name="project_name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Description</label>
                        <div class="col-md-6">
                            <textarea id="description"  class="form-control" name="description" ></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status" class="col-md-4 control-label">Status</label>
                        <div class="col-md-2">
                             <select class="form-control" id="status" name='status'>
                                <option value=0>0</option>
                                <option value=1>1</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Create
                            </button>

                            
                        </div>
                    </div>

                         

                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
                