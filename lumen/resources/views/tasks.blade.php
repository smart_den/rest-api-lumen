@extends('home')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tasks</div>

                <div class="panel-body">

                    
                    <ul>
                        @foreach($tasks as $task)
                        <li>{{$task->body}}</li>
                        @endforeach
                    </ul>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
                