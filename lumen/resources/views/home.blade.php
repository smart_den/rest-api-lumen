<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
      .main {
        height: 85vh;
      }
      .group_refs a {
        margin-right: 25px;
      }
      .glyphicon-plus{
        color: #26527A;
      }
    </style>

</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
         <div class="container">
           <!--  <a class="navbar-brand" href="/users">Users</a>
            <a class="navbar-brand" href="/tasks">Tasks</a> -->
            <ul class="nav navbar-nav navbar-left">
              <li><a href="/"> Home</a></li>
              <li><a href="/projects"><span class="glyphicon glyphicon-th-list"></span> Project</a></li>
              <!-- <li><a href="tasks"><span class="glyphicon glyphicon-time"></span> Tasks</a></li> -->
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="/register"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
              <li><a href="/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </nav>

    <main class="main">
       <div class="container">
         
         @yield('content')
      </div>
    </main>

    <footer class="container">
      <p>&copy; Company 2018</p>
    </footer>



</body>
</html>