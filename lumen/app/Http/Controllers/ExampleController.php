<?php

namespace App\Http\Controllers;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
}

























<?php

namespace App\Http\Controllers;

use App\Projects;
use Illuminate\Http\Request;
use App;

class ProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    function index(Request $request){
          /*   $projects = App\Projects::all();

             return response()->json($projects, 200); */
/*         return view('projects.projects', compact('projects')); */
       /* return "TASKS"; */
        if($request->isJson()){
            $projects = App\Projects::all();
            return response()->json($projects, 200);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []);
    }

     function createProject(Request $request){
         return view('projects.create');
     }


     function show($id){
       /*   $project = App\Projects::find($id);
         return view('projects.show', compact('project')); */
           $project = App\Projects::find($id);
            return response()->json($project, 200);

     }

    function store(Request $request){
         if($request->isJson()){

            $data = $request->json()->all();
            $project = projects::create([
                'project_name'  => $data['project_name'],
                'description' => $data['description'],
                'status' => $data['status'],
                'created_at' => time(),
                'updated_at' => time(),
            ]);

            return response()->json($project, 201);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []); 

  /*       $project = projects::create([
                'project_name'  => $_POST['project_name'],
                'description' => $_POST['description'],
                'status' => $_POST['status'],
                'created_at' => time(),
                'updated_at' => time(),
            ]);

            return redirect('projects'); */
    }


    function update(Request $request, $id){
       
              if($request->isJson()){

            $project = App\Projects::find($id);
            $project->id = $id;
            $project->project_name = $request->input('project_name');
            $project->description = $request->input('description');
            $project->status = $request->input('status');
            $project->updated_at = time();
  
            $project->save();

            return response()->json($project, 200);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []); 
    }


}
