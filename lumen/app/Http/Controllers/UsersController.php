<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;



class UsersController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    function index(){
       /*  return 'Hello Miden!'; */
        return view('home');
    }

    function users(Request $request){

        if($request->isJson()){
            $users = User::all();
            return response()->json($users, 200);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []);
        
    }

    function user($id){

        $user = User::find($id);

        return response()->json($user, 200);
    }

    function createUser(Request $request){

        if($request->isJson()){

            $data = $request->json()->all();

            $user = User::create([
                'name'  => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);

            return response()->json($user, 201);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []);
        
    }

    public function registerForm(Request $request){
        return view('register');
    }

    public function register(Request $request){

        $username = $request->input('name');
        $email = $request->input('email');
        $password = Hash::make($request->input('password'));

         $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users'
        ]);


        if($request->input('password') !== $request->input('password_confirmation')){
            $res['success'] = false;
            $res['result'] = 'Password not identical';
            return response($res);
        }

        $register = user::create([
            'name' => $username,
            'email' => $email ,
            'password' => $password
        ]);

        if($register){
            $res['success'] = true;
            $res['result'] = 'Success registration';
            $res['password'] = $password;
            /* return response($res); */
            return view('home', compact($res));
        }else{
            $res['success'] = false;
            $res['result'] = 'Failed registration';
            return response($res);
        }

    }


     function getToken(Request $request){

        if($request->isJson()){
            try{
                $data = $request->json()->all();
              /*     var_dump($data);
                die();  */

                $user = User::where('name', $data['name'])->first();

              

                if($user && Hash::check($data['password'], $user->password)){
                    return response()->json($user, 200);
                }else{
                    return response()->json(['error'=>"No content"], 406);
                }


            }catch(ModelNotFoundException $e){
                return response()->json(['error'=>"Unauthorized"], 401, []);
            }
         
        }
        
    }

}
