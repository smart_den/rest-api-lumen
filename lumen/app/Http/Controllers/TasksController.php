<?php

namespace App\Http\Controllers;

use App\Tasks;
use App\User;
use App\JWT_decode;
use Illuminate\Http\Request;
use App;
use DB;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\JWT;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */



    public function __construct()
    {
        //
    }


    function index(Request $request){

        /* $tok = json_encode($request->header('Authorization')); */
        /* $token = (new Parser())->parse($request->cookie('token')); */
        /* $token = User::getToken($request); */
        /* $token = \App\User::where ('email', $request-> input('email'))->first (); */

        $token = $request->cookie('token');
        $jwt = JWT_decode::decode($token, env('JWT_SECRET'),'HS256');
        /* dd($jwt); */

        $user_id = $jwt->sub;
        /* dd($user_id); */

        if($request->isJson()){

            switch($request->filter){
                    case "":
                        $tasks = App\Tasks::all();
                    break;
                    case "date_start":
                        $tasks = App\Tasks::date_start();
                    break;
                    case "date_end":
                        $tasks = App\Tasks::date_end();
                    break;
                    case "status":
                        $tasks = App\Tasks::status($request->filter_status);
                    break;
                    case "project":
                        $tasks = App\Tasks::project($request->number_project, $user_id);
                    break;
                    default:
                        $tasks = App\Tasks::all();
                    break;
            }
            
            return response()->json($tasks, 200);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []);
    }


    function show($id){
        $task = App\Tasks::find($id);
        $user = User::get_user_relations($task->id);

        if($user){
            $task->user_id = $user[0]->user_id;
        }
        return response()->json($task, 200);

    }


    function store(Request $request){
         if($request->isJson()){

            $data = $request->json()->all();

            $this->validate($request, [
                'task_name' => ['regex:/^[_0-9a-z ]+$/i', 'required', 'unique:tasks,task_name,NULL,project_id,project_id,'.$data['project_id']],
                'task_description' => 'regex:/^[_0-9a-z!,.?() ]+$/i',
            ]);
            $task = tasks::create([
                'task_name'  => $data['task_name'],
                'task_description' => $data['task_description'],
                'start_date' => $data['start_date'],
                'end_date' => $data['end_date'],
                'project_id' => $data['project_id'],
                'status' => $data['status'],
                'created_at' => time(),
                'updated_at' => time(),
            ]);

            if(isset($data['user_id']) ){
                User::create_user_relation($data['user_id'],  $task->id);
            }

            return response()->json($task, 201);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []); 
    }


     function update(Request $request, $id){
       
        if($request->isJson()){

            $this->validate($request, [
                'task_name' => ['regex:/^[_0-9a-z]+$/i', 'required'],
                'task_description' => 'regex:/^[_0-9a-z!,.?() ]+$/i',
            ]);

            $task = tasks::find($id);
            $task->id = $id;
            $task->task_name = $request->input('task_name');
            $task->task_description = $request->input('task_description');
            $task->start_date = $request->input('start_date');
            $task->end_date = $request->input('end_date');
            $task->project_id = $request->input('project_id');
            $task->status = $request->input('status');
            $task->updated_at = time();
            $task->save();
            
            $user = User::get_user_relations($task->id);

            if($request->input('user_id')){
                if(count($user)>0){
                    if($request->input('user_id') !== $user[0]->user_id){
                        Tasks::update_task_user_relation($request->input('user_id'),  $task->id);
                    }
                }else{
                    User::create_user_relation($request->input('user_id'),  $task->id);
                }
            }
           

            return response()->json($task, 200);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []); 
    }

    function destroy(Request $request, $id){
       $task = tasks::find($id);
       $user = User::get_user_relations($task->id);

 
        
       if(count($user) != 0){
            Tasks::delete_task_user_relation($task->id);
       }

       $task->delete(); 

       return response()->json('successfuly deleted task with id = '.$task->id, 200);
    }


}
