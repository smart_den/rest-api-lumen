<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use App\User;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Middleware;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class AuthController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email|max:255',
            'password' => 'required',
        ]);

        try {

            if (! $token = $this->jwt->attempt($request->only('email', 'password'))) {
                return response()->json(['login or password incorrect'], 404);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent' => $e->getMessage()], 500);

        }

     /*    
        $email  = $request->input('email');
        $password  = $request->input('password');
        $token = $this->jwt->attempt($request->only('email', 'password'));
        $login  =  user::where('email',  $email)->first();
        if($login->email){
            if(Hash::check($password, $login->password ) ){
                    return response($token)->json(compact('token'));  
            }
        }else{
             return response()->json(['password or login incorrect'], 404);
        } */
       
       /*  $id = Auth::user()->id;
        dd($id); */

       
           /*  $user = Auth::user()->id; */


/* $user = $jwt->user(); */

/* $usid = $this->getPayload()->get('sub'); */

/* echo "<pre>";
dd($this->jwt->user()->id); */
/* dd($this->jwt->user()); */

/* dd(new App\User); */

/* dd($token); */

        /* Запишем токен в куку */
        setcookie('token', $token);
        $cook = $request->cookie('token');

       

        return response()->json(compact('token'));
    }

    public function loginForm(Request $request){
        return view('login');
    }

}
