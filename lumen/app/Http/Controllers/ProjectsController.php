<?php

namespace App\Http\Controllers;

use App\Projects;
use App\Tasks;
use Illuminate\Http\Request;
use App;
use DB;

class ProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    function index(Request $request){

        if($request->isJson()){
            $projects = App\Projects::all();
            return response()->json($projects, 200);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []);
    }

    function show($id){
        $project = App\Projects::find($id);
 
        /* $tasks =  DB::select("select id from `tasks` where project_id = ".$id);  */
        $tasks =  App\Tasks::find_tasks($id); 

        $task_ids = [];
        foreach($tasks as $task){
             $task_ids[] = $task->id;
        }
         
        $project->tasks = $task_ids;

        return response()->json($project, 200);
    }

    function store(Request $request){
         if($request->isJson()){

             $this->validate($request, [
                'project_name' => 'regex:/^[_0-9a-z ]+$/i|required|unique:projects',
                'description' => 'regex:/^[_0-9a-z!,.?() ]+$/i',
            ]);


            $data = $request->json()->all();
            $project = projects::create([
                'project_name'  => $data['project_name'],
                'description' => $data['description'],
                'status' => $data['status'],
                'created_at' => time(),
                'updated_at' => time(),
            ]);

            return response()->json($project, 201);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []); 
    }


    function update(Request $request, $id){
       
        if($request->isJson()){

            $project = App\Projects::find($id);
            $project->id = $id;
            $project->project_name = $request->input('project_name');
            $project->description = $request->input('description');
            $project->status = $request->input('status');
            $project->updated_at = time();
            $project->save();

            return response()->json($project, 200);
        }

        return response()->json(['error'=>"Unauthorized"], 401, []); 
    }

    function destroy(Request $request, $id){
       $project = App\Projects::find($id);
       $tasks =  App\Tasks::find_tasks($project->id);
       if(count($tasks) != 0){
           foreach($tasks as $task){
              App\Tasks::delete_task_user_relation($task->id);
              App\Tasks::delete_task_project_relation($project->id);
           } 
       }

       $project->delete(); 
       return response()->json('successfuly deleted project with id = '.$project->id, 200);
    }


}
 