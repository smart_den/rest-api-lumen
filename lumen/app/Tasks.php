<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use DB;

class Tasks extends Model 
{
     public static function incomplete(){
        return static::where('completed', 0)->get(); 
    }

    public static function complete(){
        return static::where('completed', 1)->get(); 
    }
  
    public static function status($status){
        /* return static::orderBy('status', 'asc')->get();  */
        return static::where('status', $status)->get(); 
    }
    public static function date_start(){
        return static::orderBy('start_date', 'asc')->get(); 
    }
    public static function date_end(){
        return static::orderBy('end_date', 'asc')->get(); 
    }
    public static function project($number, $user_id){
       /*  return static::orderBy('project_id', 'asc')->get();  */
        return static::where('project_id', $number)->join('tasks_user_relations', 'tasks.id', '=', 'tasks_user_relations.task_id')->where('tasks_user_relations.user_id', '=', $user_id)->get(); 
    }
    public static function find_tasks($number){
       /*  return static::orderBy('project_id', 'asc')->get();  */
        return static::where('project_id', $number)->get(); 

       /*   $tasks =  DB::select("select id from `tasks` where project_id = ".$number); 
         dd($tasks); */
    }

    public static function delete_task_user_relation($task_id){
        DB::delete("delete from `tasks_user_relations` where task_id = ".$task_id);
    }
    public static function delete_task_project_relation($project_id){
        DB::delete("delete from `tasks` where project_id = ".$project_id); 
    }
    public static function update_task_user_relation($user_id, $task_id){
         DB::update("update `tasks_user_relations` set user_id =".$user_id." where  task_id=".$task_id);
    }





      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['task_name', 'task_description', 'start_date', 'end_date', 'project_id', 'status', 'created_at', 'updated_at'];

  
}