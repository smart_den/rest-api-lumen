<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;
use DB;

class User extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','password',
/*         ['email', 'match', 'pattern' => '/^[a-z]\w*$/i'] */
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return ['role'=>"Denis"];
    }

    public static function get_user_relations($task_id){
        return DB::select("select user_id from `tasks_user_relations` where task_id = ".$task_id);
    }
    public static function create_user_relation($user_id, $task_id){
        DB::insert("insert into `tasks_user_relations` (user_id, task_id) values (". $user_id .", ". $task_id .")");
    }

    
}