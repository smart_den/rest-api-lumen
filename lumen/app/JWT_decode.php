<?php

namespace App;


use Tymon\JWTAuth\Contracts\Providers\Auth;
use BadMethodCallException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Http\Parser\Parser;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Support\CustomClaims;
use Tymon\JWTAuth\Exceptions\JWTException;




use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;




/**
 * JSON Web Token implementation, based on this spec:
 * http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-06
 *
 * PHP version 5
 *
 * @category Authentication
 * @package  Authentication_JWT
 * @author   Neuman Vong <neuman@twilio.com>
 * @author   Anant Narayanan <anant@php.net>
 * @license  http://opensource.org/licenses/BSD-3-Clause 3-clause BSD
 * @link     https://github.com/firebase/php-jwt
 */
class JWT_decode extends Model 
{

    use Authenticatable, Authorizable;
    
    public static function test(){
        return 'Miden';
    }

    public static $leeway = 0;

    public static $supported_algs = array(
        'HS256' => array('hash_hmac', 'SHA256'),
        'HS512' => array('hash_hmac', 'SHA512'),
        'HS384' => array('hash_hmac', 'SHA384'),
        'RS256' => array('openssl', 'SHA256'),
    );

    /**
     * Decodes a JWT string into a PHP object.
     *
     * @param string            $jwt            The JWT
     * @param string|array|null $key            The key, or map of keys.
     *                                          If the algorithm used is asymmetric, this is the public key
     * @param array             $allowed_algs   List of supported verification algorithms
     *                                          Supported algorithms are 'HS256', 'HS384', 'HS512' and 'RS256'
     * @param string            $role
     * @return object The JWT's payload as a PHP object
     *
     * @throws DomainException              Algorithm was not provided
     * @throws UnexpectedValueException     Provided JWT was invalid
     * @throws SignatureInvalidException    Provided JWT was invalid because the signature verification failed
     * @throws BeforeValidException         Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws BeforeValidException         Provided JWT is trying to be used before it's been created as defined by 'iat'
     * @throws ExpiredException             Provided JWT has since expired, as defined by the 'exp' claim
     *
     * @uses jsonDecode
     * @uses urlsafeB64Decode
     */
    public static function decode($jwt, $key, $allowed_algs = array())
    {
     
    /*   if($_SESSION['__id']){
        $group = Yii::$app->db->createCommand('SELECT group_roles FROM `user` WHERE `id` ='.$_SESSION['__id'])->queryAll(\PDO::FETCH_ASSOC);

      } */

       /*  if (empty($key)) {
            // throw new InvalidArgumentException('Key may not be empty');


            Yii::$app->user->logout();
            return Yii::$app->response->redirect(['/']);

        } */
        $tks = explode('.', $jwt);
        if (count($tks) != 3) {
            // throw new UnexpectedValueException('Wrong number of segments');


             /*  Yii::$app->response->cookies->remove('token');
            Yii::$app->user->logout();
            return Yii::$app->response->redirect(['/']); */

        }
        list($headb64, $bodyb64, $cryptob64) = $tks;

        if (null === ($header = JWT_decode::jsonDecode(JWT_decode::urlsafeB64Decode($headb64)))) {
            // throw new UnexpectedValueException('Invalid header encoding');


           /*    Yii::$app->response->cookies->remove('token');
            Yii::$app->user->logout();
            return Yii::$app->response->redirect(['/']); */

        }
        if (null === $payload = JWT_decode::jsonDecode(JWT_decode::urlsafeB64Decode($bodyb64))) {
            // throw new UnexpectedValueException('Invalid claims encoding');

             /*  Yii::$app->response->cookies->remove('token');
              Yii::$app->user->logout();
            return Yii::$app->response->redirect(['/']); */

        }
        $sig = JWT_decode::urlsafeB64Decode($cryptob64);

        if (empty($header->alg)) {
            // throw new DomainException('Empty algorithm');

   /*            Yii::$app->response->cookies->remove('token');
              Yii::$app->user->logout();
            return Yii::$app->response->redirect(['/']); */

        }
        if (empty(self::$supported_algs[$header->alg])) {

           /*  Yii::$app->response->cookies->remove('token');
            Yii::$app->user->logout();
          return Yii::$app->response->redirect(['/']); */

        }
        if (!is_array($allowed_algs) || !in_array($header->alg, $allowed_algs)) {
            // throw new DomainException('Algorithm not allowed')

         /*    Yii::$app->user->logout();
            Yii::$app->response->cookies->remove('token');
            return Yii::$app->response->redirect(['/']); */

        }
        if (is_array($key) || $key instanceof \ArrayAccess) {
            if (isset($header->kid)) {
                $key = $key[$header->kid];
            } else {
                // throw new DomainException('"kid" empty, unable to lookup correct key');

           /*      Yii::$app->user->logout();
                Yii::$app->response->cookies->remove('token');
                return Yii::$app->response->redirect(['/']); */

            }
        }

        // Check the signature
        if (!JWT_decode::verify("$headb64.$bodyb64", $sig, $key, $header->alg)) {
            // throw new SignatureInvalidException('Signature verification failed');

             /*  Yii::$app->response->cookies->remove('token');
              Yii::$app->user->logout();
            return Yii::$app->response->redirect(['/']); */

        }

        // Check if the nbf if it is defined. This is the time that the
        // token can actually be used. If it's not yet that time, abort.

        if (isset($payload->nbf) && $payload->nbf > (time() + self::$leeway)) {
            // throw new BeforeValidException(
            //     'Cannot handle token prior to ' . date(DateTime::ISO8601, $payload->nbf)
            // );

          /*   Yii::$app->user->logout();
            Yii::$app->response->cookies->remove('token');
            return Yii::$app->response->redirect(['/']); */

        }

        // Check that this token has been created before 'now'. This prevents
        // using tokens that have been created for later use (and haven't
        // correctly used the nbf claim).
        if (isset($payload->iat) && $payload->iat > (time() + self::$leeway)) {
            // throw new BeforeValidException(
            //     'Cannot handle token prior to ' . date(DateTime::ISO8601, $payload->iat)
            // );
/* 
              Yii::$app->response->cookies->remove('token');
            Yii::$app->user->logout();
            return Yii::$app->response->redirect(['/']); */

        }


        //  var_dump($payload->exp);die();
        // Check if this token has expired.
        if (isset($payload->exp) && (time() - self::$leeway) >= $payload->exp) {
            
          /*   $t = Yii::$app->request->cookies->getValue('token');
   

              Yii::$app->user->logout();


              return Yii::$app->response->redirect(['/']); */

        }



        return $payload;
    }

    /**
     * Converts and signs a PHP object or array into a JWT string.
     *
     * @param object|array  $payload    PHP object or array
     * @param string        $key        The secret key.
     *                                  If the algorithm used is asymmetric, this is the private key
     * @param string        $alg        The signing algorithm.
     *                                  Supported algorithms are 'HS256', 'HS384', 'HS512' and 'RS256'
     * @param array         $head       An array with header elements to attach
     *
     * @return string A signed JWT
     *
     * @uses jsonEncode
     * @uses urlsafeB64Encode
     */
    public static function encode($payload, $key, $alg = 'HS512', $keyId = null, $head = null)
    {
        $header = array('typ' => 'JWT', 'alg' => $alg);
        if ($keyId !== null) {
            $header['kid'] = $keyId;
        }
        if ( isset($head) && is_array($head) ) {
            $header = array_merge($head, $header);
        }
        $segments = array();
        $segments[] = JWT_decode::urlsafeB64Encode(JWT_decode::jsonEncode($header));
        $segments[] = JWT_decode::urlsafeB64Encode(JWT_decode::jsonEncode($payload));
        $signing_input = implode('.', $segments);

        $signature = JWT_decode::sign($signing_input, $key, $alg);
        $segments[] = JWT_decode::urlsafeB64Encode($signature);

        return implode('.', $segments);
    }

    /**
     * Sign a string with a given key and algorithm.
     *
     * @param string            $msg    The message to sign
     * @param string|resource   $key    The secret key
     * @param string            $alg    The signing algorithm.
     *                                  Supported algorithms are 'HS256', 'HS384', 'HS512' and 'RS256'
     *
     * @return string An encrypted message
     *
     * @throws DomainException Unsupported algorithm was specified
     */
  

    /**
     * Verify a signature with the message, key and method. Not all methods
     * are symmetric, so we must have a separate verify and sign method.
     *
     * @param string            $msg        The original message (header and body)
     * @param string            $signature  The original signature
     * @param string|resource   $key        For HS*, a string key works. for RS*, must be a resource of an openssl public key
     * @param string            $alg        The algorithm
     *
     * @return bool
     *
     * @throws DomainException Invalid Algorithm or OpenSSL failure
     */
    private static function verify($msg, $signature, $key, $alg)
    {
      /* if($_SESSION['__id']){
        $group = Yii::$app->db->createCommand('SELECT group_roles FROM `user` WHERE `id` ='.$_SESSION['__id'])->queryAll(\PDO::FETCH_ASSOC);

      } */
        if (empty(self::$supported_algs[$alg])) {

         /*  Yii::$app->response->cookies->remove('token');
          Yii::$app->user->logout();
          return Yii::$app->response->redirect(['/']); */
        }

        list($function, $algorithm) = self::$supported_algs[$alg];
        switch($function) {
            case 'openssl':
                $success = openssl_verify($msg, $signature, $key, $algorithm);
                if (!$success) {

                    // throw new DomainException("OpenSSL unable to verify data: " . openssl_error_string());
                   /*  Yii::$app->response->cookies->remove('token');
                    Yii::$app->user->logout();
                    return Yii::$app->response->redirect(['/']); */
                } else {
                    return $signature;
                }
            case 'hash_hmac':
            default:
                $hash = hash_hmac($algorithm, $msg, $key, true);
                if (function_exists('hash_equals')) {
                    return hash_equals($signature, $hash);
                }
                $len = min(self::safeStrlen($signature), self::safeStrlen($hash));

                $status = 0;
                for ($i = 0; $i < $len; $i++) {
                    $status |= (ord($signature[$i]) ^ ord($hash[$i]));
                }
                $status |= (self::safeStrlen($signature) ^ self::safeStrlen($hash));

                return ($status === 0);
        }
    }

    /**
     * Decode a JSON string into a PHP object.
     *
     * @param string $input JSON string
     *
     * @return object Object representation of JSON string
     *
     * @throws DomainException Provided string was invalid JSON
     */
    public static function jsonDecode($input)
    {
        if (version_compare(PHP_VERSION, '5.4.0', '>=') && !(defined('JSON_C_VERSION') && PHP_INT_SIZE > 4)) {
            /** In PHP >=5.4.0, json_decode() accepts an options parameter, that allows you
             * to specify that large ints (like Steam Transaction IDs) should be treated as
             * strings, rather than the PHP default behaviour of converting them to floats.
             */
            $obj = json_decode($input, false, 512, JSON_BIGINT_AS_STRING);
        } else {
            /** Not all servers will support that, however, so for older versions we must
             * manually detect large ints in the JSON string and quote them (thus converting
             *them to strings) before decoding, hence the preg_replace() call.
             */
            $max_int_length = strlen((string) PHP_INT_MAX) - 1;
            $json_without_bigints = preg_replace('/:\s*(-?\d{'.$max_int_length.',})/', ': "$1"', $input);
            $obj = json_decode($json_without_bigints);
        }

        if (function_exists('json_last_error') && $errno = json_last_error()) {
            JWT_decode::handleJsonError($errno);
        } elseif ($obj === null && $input !== 'null') {
            // throw new DomainException('Null result with non-null input');

            /*   Yii::$app->response->cookies->remove('token');
            Yii::$app->user->logout();
            // return Yii::$app->response->redirect(['http://'.$_SERVER['HTTP_HOST']]);
            return Yii::$app->response->redirect(['/']); */
        }
        return $obj;
    }

    /**
     * Encode a PHP object into a JSON string.
     *
     * @param object|array $input A PHP object or array
     *
     * @return string JSON representation of the PHP object or array
     *
     * @throws DomainException Provided object could not be encoded to valid JSON
     */
    public static function jsonEncode($input)
    {
        $json = json_encode($input);
        if (function_exists('json_last_error') && $errno = json_last_error()) {
            JWT_decode::handleJsonError($errno);
        } elseif ($json === 'null' && $input !== null) {
            // throw new DomainException('Null result with non-null input');

         /*      Yii::$app->response->cookies->remove('token');
            Yii::$app->user->logout();
            return Yii::$app->response->redirect(['/']); */
        }
        return $json;
    }

    /**
     * Decode a string with URL-safe Base64.
     *
     * @param string $input A Base64 encoded string
     *
     * @return string A decoded string
     */
    public static function urlsafeB64Decode($input)
    {
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($input, '-_', '+/'));
    }

    /**
     * Encode a string with URL-safe Base64.
     *
     * @param string $input The string you want encoded
     *
     * @return string The base64 encode of what you passed in
     */
    public static function urlsafeB64Encode($input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }

    /**
     * Helper method to create a JSON error.
     *
     * @param int $errno An error number from json_last_error()
     *
     * @return void
     */
    private static function handleJsonError($errno)
    {
        $messages = array(
            JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
            JSON_ERROR_CTRL_CHAR => 'Unexpected control character found',
            JSON_ERROR_SYNTAX => 'Syntax error, malformed JSON'
        );
        throw new DomainException(
            isset($messages[$errno])
            ? $messages[$errno]
            : 'Unknown JSON error: ' . $errno
        );
    }

    /**
     * Get the number of bytes in cryptographic strings.
     *
     * @param string
     *
     * @return int
     */
    private static function safeStrlen($str)
    {
        if (function_exists('mb_strlen')) {
            return mb_strlen($str, '8bit');
        }
        return strlen($str);
    }
}
